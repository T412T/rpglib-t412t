const RpgSpell = require('../RpgSpell.js')

// spell qui lance une dance trop mignionne et empeche l'addversaire d'attaque pendant deux tour et qui heal le lanceur pendant 1 tour

/* eslint no-undef: "off" */
module.exports = class RpgSpellKawaii extends RpgSpell {

    constructor(player) {
        let data = {
            mana: 1000
            
        }
        super(player, data)
        this.count = 1
    }


    cast(target) {
        
        
        this.player.health += 150 

        if (this.player.mana < 1000) {
            this.player.cancelAttack(cancelAttack = true)
        

        let damage = this.player.randPower(200, 300)
        target.health -= damage

        this.consume()

        if (target.calculateResist()) {
            this.player.logger(`${this.player.name} force to dance ${target.name} but they resist!`)
            return true
        }
        
        
        target.addBeforeTurn(this.callback.bind(this))
        this.player.logger(`${this.player.name} force to dance ${target.name}…`)
        return true


    }

    

  

    callback(target) {

        this.count++

        if (this.count > 2) {
            target.logger('end Dance')
            return
        }

        target.logger(`${target.name} Is Dancing!`)
        target.cancelAttack()

        return this.callback.bind(this)
    }
  }


